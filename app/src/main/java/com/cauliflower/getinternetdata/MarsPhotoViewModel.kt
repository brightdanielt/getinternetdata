package com.cauliflower.getinternetdata

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.cauliflower.getinternetdata.network.MarsApi
import retrofit2.HttpException
import java.io.IOException

class MarsPhotoViewModel(private val repo: Repo) : ViewModel() {
    fun getPhotos() = repo.getPhotos()
}

class Repo() {
    fun getPhotos() = liveData {
        emit(MarsApiResult.Loading)
        try {
            val photos = MarsApi.retrofitService.getPhotos()
            emit(MarsApiResult.Done(photos))
        } catch (e: IOException) {
            emit(MarsApiResult.Error("IO error, plz retry", e))
        } catch (e: HttpException) {
            emit(MarsApiResult.Error("HTTP error, plz retry", e))
        }
    }
}

sealed class MarsApiResult<out T> {
    object Loading : MarsApiResult<Nothing>()
    data class Error(val msg: String, val throwable: Throwable) : MarsApiResult<Nothing>()
    data class Done<out R>(val data: R) : MarsApiResult<R>()
}